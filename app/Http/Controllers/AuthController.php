<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends ApiController
{
    /**
     * Permite crear un usuario nuevo
     */
   public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email|unique:users,email',
            'password'=>'required',
            'confirm_password'=>'required|same:password',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Error de validacion',$validator->errors(),422);
        }

        $input = $request->all();
        $input['password'] = bcrypt($request->get('password'));
        $user = User::create($input);

        $token = $user->createToken('laravel-passport')->accessToken;
        
        $data = [
            'token'=>$token,
            'user'=>$user            
        ];
        return $this->sendRespons($data,"Usuario registrado exitosa mente");

   }

   /**
    * Test de autenticacion
    */

    public function infoUser(){
        $user = Auth::user();
        
        $data = [
            "user" => $user
        ];
        return $this->sendRespons($data,"Bienvenido");
    }
}
